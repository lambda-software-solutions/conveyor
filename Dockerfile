FROM clojure:lein-alpine

ADD . /tmp
RUN cd /tmp && \
    lein build && \
    mkdir -p /srv && \
    mv ./target/server-standalone.jar /srv/conveyor.jar && \
    rm -rf /tmp/*

CMD ["java", "-jar", "/srv/conveyor.jar"]
