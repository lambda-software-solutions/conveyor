(ns user
  (:require [reloaded.repl :refer [system init start stop go reset]]
            [clojure.pprint :as pprint]
            [ragtime.jdbc :as rjdbc]
            [ragtime.repl :as ragtime]
            [taoensso.nippy :as nippy]
            [com.lambdasoftware.conveyor.server.system :as sys]
            [com.lambdasoftware.conveyor.server.router.proto :as r]
            [com.lambdasoftware.conveyor.protocol :as tcp-io]
            [manifold.stream :as ms]
            [manifold.deferred :as md]
            [aleph.tcp :as tcp]))

;; binds the 'reloaded.repl/system var to our system
(reloaded.repl/set-init! sys/system)

(defn migration-config []
  {:datastore (rjdbc/sql-database (get-in system [:database :conn]))
   :migrations (rjdbc/load-resources "migrations")})

(defn migrate-db []
  (ragtime/migrate (migration-config)))
(defn rollback-db []
  (ragtime/rollback (migration-config)))


(defn tcp-client []
  (md/chain (tcp/client {:host "127.0.0.1", :port 4455})
            #(tcp-io/wrap-encoded-duplex-stream %)))

(comment
  (go)
  (reset)

  (migrate-db)
  (rollback-db)

  (do
    ;; TODO: Prevent double-calling
    (defn create-user [{:keys [id fname lname age]} db]
      (try
        (swap! db update-in [:users] #(assoc % id {:id id
                                                   :first-name fname
                                                   :last-name lname
                                                   :age age}))
        (swap! db update-in [:ages] #(-> %
                                         (update-in [:count] inc)
                                         (update-in [:total] (partial + age))))
        (catch Exception e
          (println "CAUGHT" (.getMessage e)))))

    (defn update-age [{:keys [id age]} db]
      (let [prev-age (get-in @db [:users id :age] 0)
            diff (- age prev-age)]
        (swap! db update-in [:users id] #(assoc % :age age))
        (swap! db update-in [:ages] #(update-in % [:total] (partial + diff)))))

    (defn delete-user [{:keys [id]} db]
      (let [age (get-in @db [:users id :age] 0)]
        (swap! db update-in [:users] #(dissoc % id))
        (swap! db update-in [:ages] #(-> %
                                         (update-in [:count] dec)
                                         (update-in [:total] (fn [t] (- t age)))))))

    (defn average-age [db]
      (let [{:keys [total count]} (:ages @db)]
        (if (or (= count 0) (= total 0))
          0
          (/ total count))))

    (defn get-user [id db]
      (get-in @db [:users id]))

    (defn update-read-models [msg db]
      (when (= :event (:type msg))
        (try
          (let [{:keys [type] :as data} (-> msg :data tcp-io/unpack-payload nippy/thaw)]
            (condp = type
              :created (create-user data db)
              :age-changed (update-age data db)
              :deleted (delete-user data db)))
          (catch Exception e
            (println "Error" e)))))

    (def db (atom {:users {}
                   :ages {:count 0, :total 0}}))
    (def client @(tcp-client))

    (ms/consume #(println "MESSAGE:" %) client)
    (ms/consume #(update-read-models % db) client)
    (ms/put! client {:type :create-feed :feed "/users" :key 1})
    (ms/put! client {:type :subscribe-feed
                     :feed "/users"
                     :key 2})
    (ms/put! client {:type :create-feed :feed "/users/bill" :key 3})
    (ms/put! client {:type :create-event
                     :feed "/users/bill"
                     :data (nippy/freeze {:type :created
                                          :id "bill"
                                          :fname "William"
                                          :lname "Wallace"
                                          :age 42})
                     :key 4
                     :tmpid 0})
)

  (let [client (tcp-client)]
    @(ms/put! @client {:type :create-event
                       :feed "/users/bill"
                       :data (nippy/freeze {:type :created
                                            :id "bill"
                                            :fname "William"
                                            :lname "Wallace"
                                            :age 42})
                       :key 1
                       :tmpid 0})
    @(ms/put! @client {:type :create-event
                       :feed "/users/bill"
                       :data (nippy/freeze {:type :created
                                            :id "bill"
                                            :fname "William"
                                            :lname "Wallace"
                                            :age 42})
                       :key 2
                       :tmpid 0})
    (ms/close! @client))


  (println (average-age db))
  (println (get-user "bill" db))
  @db

  (ms/put! client {:type :create-feed :feed "/users/sally" :key 5})
  (ms/put! client {:type :begin-tx
                   :tmpid 1
                   :key 6})
  (ms/put! client {:type :create-event
                   :feed "/users/bill"
                   :data (nippy/freeze {:type :age-changed
                                        :id "bill"
                                        :age 50})
                   :key 6
                   :tmpid 1})
  (ms/put! client {:type :create-event
                   :feed "/users/sally"
                   :data (nippy/freeze {:type :created
                                        :id "sally"
                                        :fname "Sally"
                                        :lname "Watkins"
                                        :age 40})
                   :key 7
                   :tmpid 1})
  (ms/put! client {:type :commit-tx
                   :tmpid 1
                   :key 8})

  (ms/put! client {:type :get-feed
                   :feed "/users/bill"
                   :before-ts 0
                   :after-ts 0
                   :before-tx 15
                   :after-tx 0
                   :key 11})

  (println (average-age db))

  (ms/put! client {:type :create-event
                   :feed "/users/bill"
                   :data (nippy/freeze {:type :deleted
                                        :id "bill"})
                   :key 12
                   :tmpid 0})

  (println (get-user "bill" db))

  (def client-2 (tcp-client))
  @(ms/put! @client-2 {:type :create-feed :feed "/test" :key 9})
  @(ms/put! @client-2 {:type :create-event
                       :feed "/test"
                       :data "{:foo \"bar\" :domain :main}"
                       :key 10})

  (ms/close! @client-2))
