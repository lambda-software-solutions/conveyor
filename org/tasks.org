#+TODO: DEFINED STARTED(!) FINISHED(!) DELIVERED | ACCEPTED(!)
#+TODO: REJECTED | REPLACED-BY-BUG(@/!)

* Event Store MVP | [[https://www.pivotaltracker.com/story/show/111830013][111830013]]
** STARTED Create router
   CLOCK: [2016-04-05 Tue 21:42]--[2016-04-06 Wed 09:43] => 12:01
   - State "STARTED"    from "DEFINED"    [2016-01-18 Mon 17:33]

  Create a router component that can add routes and resolve routes.  A
  route can be a representation of one of the following:

  - Empty :: _Not found_
  - Feed :: A feed containing one or more *Events*
  - Event :: Some occurence that contains at least a timestamp and
             usually a payload

*** Capabilities

  | Description                           | HTTP Method | Sample URL            |
  |---------------------------------------+-------------+-----------------------|
  | Create a top-level feed               | POST        | /some-feed            |
  | Create a lower-level feed             | POST        | /some-feed/child-feed |
  | Create an event under a specific feed | POST        | /some-feed/events     |
  | Retrieve a feed                       | GET         | /some-feed            |
  | Retrieve all child feeds              | GET         | /some-feed/feeds      |

*** Misc

**** Reserved Words

  Reserved words cannot be used as event or feed titles, since they
  have a special meaning in the context of the router.

  - =events=
  - =feeds=
