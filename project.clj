(defproject com.lambdasoftware.conveyor/server "1.1.2"
  :description "Conveyor event sourcing database"
  :url "https://gitlab.com/lambda-software-solutions/conveyor"
  :license{:name "Eclipse Public License"
           :url "http://www.eclipse.org/legal/epl-v10.html"}
  :repl-options {:init-ns user}
  :plugins [[lein-auto "0.1.3"]]
  :dependencies [[org.clojure/clojure "1.9.0-alpha9"]
                 ;; TCP/Web server
                 [aleph "0.4.1"]
                 [manifold "0.1.4"]

                 ;; Web server
                 [yada "1.2.0"]
                 [bidi "2.0.12"]
                 [prismatic/schema "1.1.4"]
                 [com.taoensso/nippy "2.13.0"]
                 [com.cemerick/url "0.1.1"]

                 ;; TCP server
                 [gloss "0.2.5"]
                 [com.lambdasoftware.conveyor/protocol "1.0.0"]

                 ;; Framework
                 [com.stuartsierra/component "0.3.1"]
                 [org.danielsz/system "0.2.0"]
                 [reloaded.repl "0.2.1"]

                 ;; Config
                 [aero "1.1.3"]

                 ;; HTTP client
                 [clj-http "3.7.0"]

                 ;; Util
                 [clj-time "0.11.0"]
                 [overtone/at-at "1.2.0"]
                 [kendru/perseverance "0.1.2"]
                 [com.taoensso/encore "2.88.1"]
                 [com.taoensso/timbre "4.8.0"]
                 [crypto-random "1.2.0"]

                 ;; Database
                 [org.clojure/java.jdbc "0.6.1"]
                 [org.postgresql/postgresql "42.1.1.jre7"]
                 [com.jolbox/bonecp "0.8.0.RELEASE"]
                 [org.clojure/java.jdbc "0.6.1"]
                 [org.slf4j/slf4j-nop "1.7.21"]
                 [ragtime "0.6.0"]]
  :main com.lambdasoftware.conveyor.server.core
  :uberjar-name "server-standalone.jar"
  :aliases {"build" ["do" ["clean"] ["compile"] ["uberjar"]]}

  :profiles {:uberjar {:aot :all}
             :dev {:source-paths ["src" "dev"]
                   :dependencies [[org.clojure/test.check "0.9.0"]
                                  [com.gfredericks/test.chuck "0.2.5"]]}})
