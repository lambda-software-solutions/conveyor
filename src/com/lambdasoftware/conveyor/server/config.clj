(ns com.lambdasoftware.conveyor.server.config
  (:require [aero.core :as aero]
            [clojure.java.io :as io]))

(defn config []
  (aero/read-config (io/resource "config.edn")))

(defn www-opts [config]
  (:www config))

(defn tcp-opts [config]
  (:tcp config))

(defn database-opts [config]
  (:database config))

(defn router-opts [config]
  (:router config))

(defn scheduler-opts [config]
  (:scheduer config))
