(ns com.lambdasoftware.conveyor.server.core
  (:gen-class)
  (:require [com.lambdasoftware.conveyor.server.system :refer [system]]
            [com.stuartsierra.component :as component]
            [ragtime.jdbc :as rjdbc]
            [ragtime.repl :as ragtime]
            [perseverance.core :as perseverance :refer [retriable retry]])
  (:import [java.sql SQLException]))

(defn migrate-db [sys]
  (ragtime/migrate {:datastore (rjdbc/sql-database (get-in sys [:database :conn]))
                    :migrations (rjdbc/load-resources "migrations")}))

(defn retriable-migrate-db
  [sys]
  (retriable {:catch [SQLException]
              :tag ::connection}
             (migrate-db sys)))

(defn -main []
  (let [sys (component/start (system))]
    (retry {:strategy (perseverance/progressive-retry-strategy :initial-delay 1000
                                                               :stable-length 2
                                                               :multiplier 2
                                                               :max-delay 30000)}
           (retriable-migrate-db sys))))
