(ns com.lambdasoftware.conveyor.server.infr.db
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log])
  (:import [com.jolbox.bonecp BoneCPDataSource]))

(defrecord ConnectionPool [opts]
  component/Lifecycle
  (start [component]
    (let [{:keys [host port user password database
                  init-pool-size max-pool-size idle-time partitions]} opts
          jdbc-url (format "jdbc:postgresql://%s:%d/%s" host port database)
          _ (log/info (format "Starting database connection pool: %s" jdbc-url))
          min-connections (inc (quot init-pool-size partitions))
          max-connections (inc (quot max-pool-size partitions))
          pooled-conn (doto (BoneCPDataSource.)
                        (.setDriverClass "org.postgresql.Driver")
                        (.setJdbcUrl jdbc-url)
                        (.setUsername user)
                        (.setPassword password)
                        (.setMinConnectionsPerPartition min-connections)
                        (.setMaxConnectionsPerPartition max-connections)
                        (.setPartitionCount partitions)
                        (.setStatisticsEnabled true)
                        (.setIdleMaxAgeInMinutes idle-time))
          jdbc-conn {:datasource pooled-conn}]
      (assoc component :conn jdbc-conn)))
  (stop [component]
    (log/info "Stopping database connection pool")
    (.close (get-in component [:conn :datasource]))
    (dissoc component :conn)))

(defn new-connection-pool [opts]
  (->ConnectionPool opts))
