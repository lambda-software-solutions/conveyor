(ns com.lambdasoftware.conveyor.server.infr.sched
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [overtone.at-at :as at-at]))

(defprotocol Scheduler
  (recurring-task [this task-name interval task-fn])
  (cancel [this task-name]))

(defrecord DefaultScheduler [opts sched-pool tasks]
  component/Lifecycle
  (start [component]
    (log/info "Starting scheduler")
    (assoc component
           :sched-pool (at-at/mk-pool
                        :cpu-count (get opts :threads 2))
           :tasks (atom {})))

  (stop [component]
    (log/info "Stopping scheduler")
    (at-at/stop-and-reset-pool! (:sched-pool component)
                                :strategy :kill)
    (assoc component
           :sched-pool nil
           :tasks nil))

  Scheduler
  (recurring-task [this task-name interval task-fn]
    (log/debug (str "Scheduling task, " (name task-name) ", every " interval) "ms")
    (let [task (at-at/every interval task-fn sched-pool)]
      (swap! tasks assoc task-name task)
      task))

  (cancel [this task-name]
    (log/debug "Cancelling task," (name task-name))
    (at-at/stop (get tasks task-name))))

(defn new-scheduler [opts]
  (map->DefaultScheduler {:opts opts}))
