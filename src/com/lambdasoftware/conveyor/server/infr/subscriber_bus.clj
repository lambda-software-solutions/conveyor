(ns com.lambdasoftware.conveyor.server.infr.subscriber-bus
  (:require [com.lambdasoftware.conveyor.server.trie :as trie]
            [taoensso.timbre :as log]
            [clojure.java.jdbc :as jdb]
            [taoensso.nippy :as nippy]
            [clj-http.client :as http]
            [perseverance.core :as perseverance :refer [retriable retry]]
            [manifold.stream :as s]
            [com.stuartsierra.component :as component])
  (:import (org.postgresql.util PSQLException)))


(declare unsubscribe-all*)

(defn postback-webhook* [url event]
  (http/post url {:form-params event
                  :content-type :json
                  :socket-timeout 3000
                  :conn-timeout 5000}))


(defn postback-webhook [url event]
  (retriable {} (postback-webhook* url event)))


(defn webhook-subscriber [subscriber-bus url]
  (let [buffer (s/buffered-stream 1000)]
    (s/consume
     (fn [event]
       (try
         (retry {:strategy (perseverance/progressive-retry-strategy :initial-delay 2000
                                                                    :max-delay 30000
                                                                    :max-count 15)
                 :log-fn (fn [ex attempt delay]
                           (log/debug (format "Webhook request to %s failed. Performing atempt %d after %dms"
                                              url attempt delay)))}
                (postback-webhook url (update event :data nippy/thaw)))
         (catch Exception e
           (log/error e "Request error processing event for subscription. Not retrying, since this appears to be related to an invalid webhook endpoint.")
           (when-let [{:keys [status]} (ex-data e)]
             (when (= 404 status)
               (log/info "Subscriber endpoint returned 404 - removing subscriber")
               (unsubscribe-all* subscriber-bus
                 {:type :subscribers/webhook
                  :key (str "w:" url)}))))))
     buffer)
    {:type :subscribers/webhook
     :key (str "w:" url)
     :url url
     :stream buffer}))

(defn stream-subscriber [stream id]
  {:type :subscribers/stream
   :key (str "s:" id)
   :stream stream})


(defmulti subscriber-from
  (fn [subscriber-bus data] (:type data)))

(defmethod subscriber-from :subscribers/webhook
  [subscriber-bus data]
  (webhook-subscriber subscriber-bus (:url data)))


(defn add-path-to-subscriptions
  "Given a subscriber and a path, returns an index-updating function that will ensure that the
  subscriber is indexed and contains path among its subscriptions."
  [subscriber path]
  (fn [idx]
    (update-in idx [(:key subscriber)]
               (fnil #(update-in % [:subscriptions] conj path)
                     (assoc subscriber :subscriptions #{})))))

(defn remove-path-from-subscriptions
  "Given a subscriber and a path, returns an index-updating function that will ensure that the
  subscriber specified is not subscribed to the path given."
  [subscriber path]
  (fn [idx]
    (if-let [subscriber (get idx (:key subscriber))]
      (assoc idx (:key subscriber)
             (update subscriber :subscriptions disj path))
      idx)))


(defn load-persisted-subscriptions! [subscriber-bus db]
  (try
    (let [start (System/currentTimeMillis)
          subscriptions (jdb/query db ["SELECT * FROM subscriptions"])]
      (log/info (format "Loaded %d subscriptions in %.3fs"
                        (count subscriptions)
                        (/ (- (System/currentTimeMillis) start) 1000.)))
      (reduce (fn [[by-path subscriptions] {:keys [:key :path :subscriber]}]
                (let [path (nippy/thaw path)
                      subscriber (subscriber-from subscriber-bus (nippy/thaw subscriber))]
                  [((add-path-to-subscriptions subscriber path) by-path)
                   (trie/add-element subscriptions path key)]))
              [{} (trie/mk-trie)]
              subscriptions))
    (catch PSQLException e
      (log/error e "Error loading initial subscriptions"))))

(defn maybe-persist-subscription! [db subscriber path]
  (when (= :subscribers/webhook (:type subscriber))
    (log/debug (format "Persisting subscription to db: %s -> %s" (:key subscriber) path))
    (jdb/insert! db "subscriptions" {:key (:key subscriber)
                                     :path (nippy/freeze path)
                                     :subscriber (nippy/freeze subscriber)})))


(defn maybe-unpersist-subscription! [db subscriber path]
  (when (= :subscribers/webhook (:type subscriber))
    (log/debug (format "Removing subscription from db: %s -> %s" (:key subscriber) path))
    (jdb/delete! db "subscriptions" ["\"key\" = ? AND \"path\" = ?" (:key subscriber) (nippy/freeze path)])))


(defn emit-to-subscriber! [subscriber event]
  (s/put! (:stream subscriber) event))


(defn unsubscribe-all* [subscriber-bus subscriber]
  (let [k (:key subscriber)
        {:keys [subscribers  subscriptions database]} subscriber-bus]
    (log/debug (format "Cancelling all subscriptions for subscriber %s" k))
    (when-let [paths (get-in @subscribers [k :subscriptions])]
      (dosync
        (alter subscribers dissoc k)
        (alter subscriptions
              (fn [subs]
                (reduce (fn [t path] (trie/remove-element t path k)) subs paths))))
      ;; Performing a delete per path is not optimal, and this could be optimized to a batch delete
      (doseq [path paths]
        (maybe-unpersist-subscription! (:conn database) subscriber path)))))


(defprotocol ISubscriberBus
  (subscribe [this path subscriber])
  (unsubscribe [this path subscriber])
  (unsubscribe-all [this subscriber])
  (get-subscriptions [this subscriber])
  (emit! [this path event]))

(defrecord SubscriberBus [database subscribers subscriptions]
  component/Lifecycle
  (start [this]
    (log/info "Starting subscriber message bus")
    (let [[subscribers subscriptions] (load-persisted-subscriptions! this (:conn database))]
      (assoc this
             :subscriptions (ref subscriptions)
             :subscribers (ref subscribers))))

  (stop [this]
    (log/info "Stopping subscriber message bus")
    (assoc this
           :subscribers nil
           :subscriptions nil))

  ISubscriberBus
  (subscribe [this path subscriber]
    (log/debug (format "Registering subscriber %s at %s" (:key subscriber) path))
    (dosync
     (alter subscribers (add-path-to-subscriptions subscriber path))
     (alter subscriptions trie/add-element path (:key subscriber)))
    (maybe-persist-subscription! (:conn database) subscriber path)
    nil)

  (unsubscribe [this path subscriber]
    (log/debug (format "Cancelling subscriber %s at %s" (:key subscriber) path))
    (dosync
     (alter subscribers (remove-path-from-subscriptions subscriber path))
     (alter subscriptions trie/remove-element path (:key subscriber)))
    (maybe-unpersist-subscription! (:conn database) subscriber path)
    nil)

  (unsubscribe-all [this subscriber]
    (unsubscribe-all* this subscriber))

  (get-subscriptions [this subscriber]
    (get-in @subscribers [(:key subscriber) :subscriptions]))

  (emit! [this path event]
    (let [subscribers-by-key @subscribers
          subscribers (map #(get subscribers-by-key %)
                           (into #{} (trie/collect @subscriptions path)))]
      (doseq [s subscribers]
        (emit-to-subscriber! s event)))))
    

(defn new-subscriber-bus []
  (map->SubscriberBus {}))

