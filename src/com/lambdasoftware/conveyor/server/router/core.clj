(ns com.lambdasoftware.conveyor.server.router.core
  (:require [com.lambdasoftware.conveyor.server.router.proto :refer :all]
            [com.lambdasoftware.conveyor.server.router.memory :refer [map->MemoryRouter feed]]
            [com.lambdasoftware.conveyor.server.router.sql :refer [map->SqlRouter]]))

(defn uuid [] (str (java.util.UUID/randomUUID)))

(defn segments [path]
  {:pre [(= '\/ (.charAt path 0))]}
  (clojure.string/split (.substring path 1) #"/"))

(defn parse-event [parts]
  (let [feed (into [] (butlast parts))]
    {:type :event
     :feed feed}))

(defn parse-subfeeds [parts]
  (let [feed (into [] (butlast parts))]
    {:type :subfeeds
     :feed feed}))

(defn parse-feed [parts]
  {:type :feed
   :feed parts})

(defn parse [path]
  (let [parts (segments path)
        final (last parts)]
    (cond
      (= "events" final)
      (parse-event parts)
      (= "feeds" final)
      (parse-subfeeds parts)
      :else
      (parse-feed parts))))

(defmulti new-router
  (fn [opts]
    (println "Creating from opts" opts)
    (:type opts)))

(defmethod new-router :memory
  [{:keys [routes]
    :or {routes (atom (feed))}}]
  (map->MemoryRouter {:routes routes}))

(defmethod new-router :sql
  [opts]
  (map->SqlRouter {}))
