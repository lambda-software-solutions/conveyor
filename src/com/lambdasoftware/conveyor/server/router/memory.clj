(ns com.lambdasoftware.conveyor.server.router.memory
  (:refer-clojure :exclude [empty])
  (:require [clojure.string :refer [join]]
            [com.lambdasoftware.conveyor.server.util :as util]
            [com.lambdasoftware.conveyor.server.router.proto :refer :all]
            [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [schema.core :as s]))

(defn feed
  ([] (feed {}))
  ([subfeeds]
   {:type :events/feed
    :events []
    :feeds subfeeds}))

(defn empty []
  {:type :events/empty})

(defn event [e]
  {:type :events/event
   :data e})

(defn feed-ref [segments]
  {:type :events/feed-ref
   :url (str "/" (join "/" segments))})

(comment
  "I do not remember what these schemas were added for. If anything, they probably belong
  in a higher layer of the application"

  (s/defschema Event
    {:id s/Uuid
     :time Long})

  (s/defschema Feed
    {:links {:self s/Str
             (s/optional-key :prev) s/Str
             (s/optional-key :next) s/Str
             :feeds [s/Str]}
     :data [Event]}))

(defn- add-feed [routes path]
  (let [routes (if (empty? routes) (feed) routes)]
    (if (empty? path)
      routes
      (if (= :events/feed (get-in routes [:feeds (first path) :type]))
        (update-in routes
                   [:feeds (first path)]
                   add-feed (rest path))
        (assoc-in routes [:feeds (first path)]
                  (add-feed (feed) (rest path)))))))

(defn- nested-feed-path [path]
  (into [] (interleave (repeat :feeds) path)))

(defn nested-feed [routes path]
  (let [full-path (nested-feed-path path)
        found-feed (get-in routes full-path)]
    (when (and found-feed
               (= :events/feed (:type found-feed)))
      found-feed)))

(defn- add-event [routes path event]
  (when [feed (nested-feed routes path)]
    (let [evt-path (conj (nested-feed-path path) :events)]
      (update-in routes evt-path conj event))))

(defn- sub-events [feed]
  (let [events (:events feed)]
    (if (empty? (:feeds feed))
      events
      (concat events (mapcat sub-events (vals (:feeds feed)))))))

(defn- get-events [routes path]
  (when-let [feed (nested-feed routes path)]
    (sub-events feed)))

(defn sub-feeds [routes path]
  (when-let [feed (nested-feed routes path)]
    (mapv #(feed-ref (conj path %))
          (keys (:feeds feed)))))

(defn meets-criteria [evt criteria]
  (let [{:keys [ts tx-id]} evt
        {:keys [before-ts after-ts before-tx after-tx]
         :or {before-ts 0
              after-ts 0
              before-tx 0
              after-tx 0}} criteria]
    (and (or (= 0 before-ts) (< ts before-ts))
         (or (= 0 after-ts) (> ts after-ts))
         (or (= 0 before-tx) (< tx-id before-tx))
         (or (= 0 after-tx) (> tx-id after-tx)))))

(defrecord MemoryRouter [routes]
  component/Lifecycle
  (start [component]
    (log/info "Starting in-memory router")
    (assoc component :routes routes))
  (stop [component]
    (log/info "Stopping in-memory router")
    (dissoc component :routes))

  Router
  (add-feed! [this path]
    (swap! (:routes this) add-feed path))
  (emit-event! [this path e]
    (swap! (:routes this) add-event path (event e)))
  (emit-events! [this events]
    (swap! (:routes this)
           #(reduce (fn [routes [path e]]
                      (add-event routes path (event e)))
                    % events)))
  (get-feed [this path criteria {:keys [page per-page] :as pagination}]
    (let [events (->> (get-events (deref (:routes this)) path)
                      (filter #(meets-criteria % criteria)))]
      {:events (util/paginate page per-page events)
       :total (count events)}))
  (get-subfeeds [this path {:keys [page per-page] :as pagination}]
    (let [feeds (sub-feeds (deref (:routes this)) path)]
      {:feeds (util/paginate page per-page feeds)
       :total (count feeds)})))
