(ns com.lambdasoftware.conveyor.server.router.proto)

;; TODO: Add transactions to the base router protocol. They are currently handled in each
;; adapter separately. We need the ability to create, append event, commit, and rollback
(defprotocol Router
  (add-feed! [this url])
  (emit-event! [this url event-data])
  (emit-events! [this events]) ; events is pair of [url event-data]
  (get-feed [this url criteria pagination])
  (get-subfeeds [this url pagination]))
