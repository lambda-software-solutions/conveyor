(ns com.lambdasoftware.conveyor.server.router.sql
  "An SQL-backed router implementation that uses a nested set hierarchy
  for feeds to enable fast reads. Event data is treated as byte arrays."
  (:require [com.stuartsierra.component :as component]
            [com.lambdasoftware.conveyor.server.router.proto :refer :all]
            [com.lambdasoftware.conveyor.server.infr.subscriber-bus :as bus]
            [taoensso.timbre :as log]
            [clojure.java.jdbc :as jdb]
            [clojure.string :as string])
  (:import [org.postgresql.util PSQLException]))

(def queries {:get-events "SELECT * FROM events WHERE %s ORDER BY created ASC"
              :count-events "SELECT COUNT(*) AS c FROM events WHERE %s LIMIT 1"
              :get-subfeeds "SELECT * FROM feeds WHERE parent_mpath = ? ORDER BY mpath"
              :count-subfeeds "SELECT COUNT(*) AS c FROM feeds WHERE parent_mpath = ? LIMIT 1"
              :feed-exists? "SELECT * FROM feeds WHERE mpath = ? LIMIT 1"
              :next-tx-id "SELECT nextval('tx_ids')"})

(defn extract-count
  "Gets the count out query results from a query like \"SELECT COUNT(*) AS c ...\""
  [results]
  (Integer. (get (first results) :c 0)))

(defn path-exists? [db path]
  (seq
   (jdb/query db
              [(:feed-exists? queries) path]
              {:row-fn :mpath})))

(defn ensure-path-exists! [db path parent-path]
  (when-not (path-exists? db path)
    (jdb/insert! db :feeds
                {:mpath path
                 :parent_mpath parent-path})))

(defn format-event [evt]
  (let [{:keys [id tx_id created feed_mpath payload]} evt]
    {:type :events/event
     :id id
     :tx-id tx_id
     :timestamp created
     :feed feed_mpath
     :data payload}))

(defn path-segments [path]
  (filter seq (string/split path #"/")))

(defn pagination-clause [{:keys [page per-page]}]
  (str "LIMIT " per-page (when (> page 1) (str " OFFSET " (* per-page (dec page))))))

(defn- get-feed-wheres
  "Gets pairs of [where-clause parameter] for each condition"
  [path {:keys [before-ts after-ts before-tx after-tx]
         :or {before-ts 0, after-ts 0
              before-tx 0, after-tx 0}
         :as criteria}]
  (cond-> [["feed_mpath LIKE ?" (str path "%")]]
    (> before-ts 0) (conj ["created < to_timestamp(?)" before-ts])
    (> after-ts 0) (conj ["created > to_timestamp(?)" after-ts])
    (> before-tx 0) (conj ["tx_id < ?" before-tx])
    (> after-tx 0) (conj ["tx_id > ?" after-tx])))

(defn- get-feed-query* [path criteria]
  (let [wheres (get-feed-wheres path criteria)
        where-str (clojure.string/join " AND " (map first wheres))
        params (mapv second wheres)]
    (into [(format (:get-events queries) where-str)] params)))

(defn get-feed-query
  "Get the query vector of [sql param*] for the feed query, building
  the appropriate WHERE clause for the ts and/or tx bounds specified."
  [path criteria pagination]
  (update-in (get-feed-query* path criteria) [0]
             str " " (pagination-clause pagination)))

(defn get-event-count-query
  "Get the query vector of [sql param*] for the event count query
  with the appropriate WHERE clause added."
  [path criteria]
  (let [wheres (get-feed-wheres path criteria)
        where-str (clojure.string/join " AND " (map first wheres))
        params (mapv second wheres)]
    (into [(format (:count-events queries) where-str)] params)))

(defn get-subfeeds-query [path pagination]
  [(str (:get-subfeeds queries) " " (pagination-clause pagination))
   path])

(defn next-tx-id [db]
  (-> (jdb/query db [(:next-tx-id queries)])
      first
      :nextval))

(defn save-event!
  ([db path evt] (save-event! db path evt nil))
  ([db path evt tx-id]
   (let [evt-data (cond-> {:feed_mpath path, :payload evt}
                    tx-id (assoc :tx_id tx-id))]
     (-> (jdb/insert! db :events evt-data)
         first
         format-event))))

(defn commit-all-events! [conn subscriber-bus events]
  (jdb/with-db-transaction [tx conn]
    (try
      (doall
       (for [[path evt & [tx-id]] events
             :let [id (or tx-id (next-tx-id tx))
                   e (save-event! tx path evt id)]]
         (do
           (bus/emit! subscriber-bus (path-segments path) e)
           e)))
      (catch PSQLException e
        (log/error e "Error inserting events")
        nil))))

(defrecord SqlRouter [database subscriber-bus]
  component/Lifecycle
  (start [component]
    (log/info "Starting db-backed router")
    component)
  (stop [component]
    (log/info "Stopping db-backed router")
    component)

  Router
  (add-feed! [this path]
    ;; TODO: Find the portion of the path that does not exist and
    ;; insert as single transaction
    (let [db (:conn database)]
      (loop [segments (path-segments path)
             parent-path ""]
        (when (seq segments)
          (let [current-path (str parent-path "/" (first segments))]
            (ensure-path-exists! db current-path parent-path)
            (recur (rest segments) current-path))))))

  (emit-event! [this path evt]
    (let [db (:conn database)]
      (try
        (let [e (save-event! db path evt)]
          (bus/emit! subscriber-bus (path-segments path) e)
          e)
        (catch PSQLException e
          (log/error e "Error inserting event")
          nil))))

  (emit-events! [this events]
    (commit-all-events! (:conn database) subscriber-bus events))

  (get-feed [this path criteria pagination]
    (let [db (:conn database)
          events (jdb/query db (get-feed-query path criteria pagination))
          event-count (->> (get-event-count-query path criteria)
                           (jdb/query db)
                           (extract-count))]
      {:type :events/feed
       :feed path
       :total event-count
       :events (map format-event events)}))

  (get-subfeeds [this path pagination]
    (let [db (:conn database)
          feeds (->> (jdb/query db (get-subfeeds-query path pagination))
                     (map :mpath))
          feed-count (extract-count (jdb/query db [(:count-subfeeds queries) path]))]
      {:type :events/feed
       :feeds feeds
       :total feed-count})))
