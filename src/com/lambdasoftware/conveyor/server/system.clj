(ns com.lambdasoftware.conveyor.server.system
  (:require [com.stuartsierra.component :as component]
            [com.lambdasoftware.conveyor.server.router.core :refer [new-router]]
            [com.lambdasoftware.conveyor.server.www.core :refer [new-server]]
            [com.lambdasoftware.conveyor.server.tcp.core :refer [tcp-server]]
            [com.lambdasoftware.conveyor.server.infr.db :refer [new-connection-pool]]
            [com.lambdasoftware.conveyor.server.infr.sched :refer [new-scheduler]]
            [com.lambdasoftware.conveyor.server.infr.subscriber-bus :refer [new-subscriber-bus]]
            [com.lambdasoftware.conveyor.server.config :as config]))

(defn system []
  (let [cfg (config/config)]
    (component/system-map
     :database (new-connection-pool (config/database-opts cfg))
     :subscriber-bus (component/using
                      (new-subscriber-bus)
                      [:database])
     :router (component/using
              (new-router (config/router-opts cfg))
              [:database :subscriber-bus])
     :scheduler (new-scheduler (config/scheduler-opts cfg))
     :tcp (component/using
           (tcp-server (config/tcp-opts cfg))
           [:router :scheduler :subscriber-bus])
     :www (component/using
           (new-server (config/www-opts cfg))
           [:router :database :subscriber-bus]))))
