(ns com.lambdasoftware.conveyor.server.tcp.core
     (:require [com.lambdasoftware.conveyor.protocol :as protocol]
               [gloss.io :as gio]
               [com.lambdasoftware.conveyor.server.router.proto :as routes]
               [com.lambdasoftware.conveyor.server.infr.sched :as sched]
               [com.lambdasoftware.conveyor.server.infr.subscriber-bus :as bus]
               [com.stuartsierra.component :as component]
               [taoensso.timbre :as log]
               [aleph.tcp :as tcp]
               [manifold.deferred :as d]
               [manifold.stream :as s])
     (:import [java.net Inet6Address InetSocketAddress]))


(def max-session-inactive-ms (* 60 1000))


(def id-gen (atom 0))
(defn- next-id [] (swap! id-gen inc))

(defn connection []
  {:id (next-id)
   :out (s/stream 1000)
   :transactions {}
   :message-count 0
   :last-message (System/currentTimeMillis)})


(defn event->msg [event]
  (let [{:keys [timestamp data feed tx-id]} event]
    {:type :event
     :feed feed
     :ts (int (/ (.getTime timestamp) 1000))
     :tx-id tx-id
     :data (gio/to-byte-buffer data)
     :key 0}))


(defn segments [path]
  {:pre [(= '\/ (.charAt path 0))]}
  (clojure.string/split (.substring path 1) #"/"))


(defn unsubscribe-stream!
  [subscriber-bus id connections]
  (when-let [conn (get @connections id)]
    (bus/unsubscribe-all subscriber-bus (bus/stream-subscriber (:out conn) id))
    (s/close! (:out conn))
    (swap! connections dissoc id)))


(defn cull-inactive! [connections subscriber-bus]
  (let [now (System/currentTimeMillis)]
    (doseq [c (vals @connections)
            :let [{:keys [id last-message out]} c
                  idle-ms (- now last-message)]]
      (when (> idle-ms max-session-inactive-ms)
        (try
          (log/info (format "Disconnecting client, %d, after %ds of inactivity"
                            id (int (/ idle-ms 1000))))
          (s/put! out {:type :server-disconnect
                       :message "Timed out due to inactivity"})
          (s/close! out)
          (unsubscribe-stream! subscriber-bus id connections)
          (catch Exception e
            (log/error e "Caught exception in inactive session culling")))))))


;; Note that a handler can return nil if it does not want to issue a reply
(defmulti handle-request
  (fn [msg out connections router subscriber-bus id]
    (:type msg)))

(defmethod handle-request :create-event
  [msg out connections router _ id]
  (let [{:keys [feed key tmpid]} msg
        data (protocol/unpack-payload (:data msg))]
    (if (= 0 tmpid)
      (if-let [event (routes/emit-event! router feed data)]
        {:type :event-created
         :feed feed
         :tx-id (:tx-id event)
         :key key}
        {:type :error
         :code :feed-not-found
         :message (format "Feed, %s , does not exist" feed)
         :key key})
      (do
        ;; Queue event to be committed later
        (swap! connections update-in [id :transactions tmpid]
               conj {:event data :feed feed :key key})
        nil))))

(defmethod handle-request :begin-tx
  [msg out connections _ _ id]
  (swap! connections assoc-in [id :transactions (:tmpid msg)] [])
  nil)

(defmethod handle-request :commit-tx
  [msg out connections router _ id]
  (when-let [inserts (get-in @connections [id :transactions (:tmpid msg)])]
    (when-let [events (routes/emit-events! router (map (juxt :feed :event) inserts))]
      ;; Combine returned events with keys from original messages
      (doseq [[key event] (map vector (map :key inserts) events)
              :let [{:keys [feed tx-id]} event]]
        (s/put! out {:type :event-created
                     :feed feed
                     :tx-id tx-id
                     :key key}))
      nil)))

(defmethod handle-request :rollback-tx
  [msg out connections _ _ id]
  (swap! connections update-in [id :transactions] dissoc (:tmpid msg))
  nil)

;; Note that this implentation does not include pagination and will only fetch
;; up to 10K events. This should be fixed in this project, the protocol, and
;; the client in the same release.
(defmethod handle-request :get-feed
  [msg _ _ router _ _]
  (let [{:keys [key feed before-ts after-ts before-tx after-tx]
         :or {before-ts 0
              after-ts 0
              before-tx 0
              after-tx 0}} msg
        feed (routes/get-feed router feed
                              {:before-ts before-ts
                               :after-ts after-ts
                               :before-tx before-tx
                               :after-tx after-tx}
                              {:page 1 :per-page 10000})]
    {:type :feed
     :feed (:feed msg)
     :events (map #(-> % event->msg (dissoc :type :count))
                  (:events feed))
     :key key}))

(defmethod handle-request :create-feed
  [msg _ _ router _ _]
  (let [{:keys [feed key]} msg]
    (try
      (routes/add-feed! router feed)
      (log/info (format "Feed created: %s" feed))
      (catch Exception e
        (log/error e (format "Error creating feed: %s" feed))))
    {:type :feed-created
     :feed feed
     :key key}))

(defmethod handle-request :subscribe-feed
  [msg out connections _ subscriber-bus id]
  (let [{:keys [feed key before-ts after-ts before-tx after-tx]
         :or {before-ts 0
              after-ts 0
              before-tx 0
              after-tx 0}} msg]
    (bus/subscribe subscriber-bus
                   (segments feed)
                   (bus/stream-subscriber (get-in @connections [id :out]) id))
    {:type :subscribed-to-feed
     :feed feed
     :key key}))

(defmethod handle-request :client-disconnect
  [_ out connections _ subscriber-bus id]
  (log/info (format "Disconnect requested from client, %d" id))
  (s/put! out {:type :server-disconnect
               :message "Acknowledged client disconnect"})
  (unsubscribe-stream! subscriber-bus id connections)
  nil)

(defmethod handle-request :heartbeat
  [msg _ _ _ _ _]
  {:type :heartbeat
   :order (:order msg)})


(defn try-handle-request [msg out connections router subscriber-bus id]
  (try
    (swap! connections update-in [id]
           #(-> %
                (update :message-count inc)
                (assoc :last-message (System/currentTimeMillis))))
    (handle-request msg out connections router subscriber-bus id)
    (catch Exception e
      (log/warn e "Error processing request")
      {:type :error
       :code :unknown-error
       :message (.getMessage e)
       :key (:key msg)})))


(defn tcp-handler [connections router subscriber-bus]
  (fn [sock info]
    (let [c (connection)]
      ;; Register the new connection with the server
      (swap! connections assoc (:id c) c)

      ;; Connect the stream for this connection to the TCP socket stream
      (s/consume
         (fn [e]
           (when (= :events/event (:type e))
             (try
               (s/put! sock (event->msg e))
               (catch Exception e
                 (log/error e "Error converting event to message")))))
         (:out c))

      ;; Since a lot of the handlers operate in request->response mode, we allow
      ;; them to return a message and pipe that back to the TCP socket stream
      (s/connect
       (s/filter
        some?
        (s/map #(try-handle-request % sock connections router subscriber-bus (:id c))
               sock))
       sock
       "Message response channel"))))

(defrecord TcpServer [port ip cull-interval connections tcp-server router scheduler subscriber-bus]
  component/Lifecycle
  (start [component]
    (log/info (format "Starting TCP server on %s:%d" ip port))
    (let [ip (Inet6Address/getByName ip) ;; Will also resolve Inet4Address
          addr (InetSocketAddress. ip port)
          tcp-server (tcp/start-server
                      (fn [sock info]
                        (log/info (format "Received connection from %s on %s:%s"
                                          (:remote-addr info) (:server-name info) (:server-port info)))
                        (let [handler (tcp-handler connections router subscriber-bus)
                              wrapped-socket (protocol/wrap-encoded-duplex-stream sock)]
                          (handler wrapped-socket info)))
                      {:socket-address addr})]
      (sched/recurring-task scheduler :cull-connections
                            cull-interval #(cull-inactive! connections subscriber-bus))
      (assoc component :tcp-server tcp-server)))

  (stop [component]
    (log/info "Disconnecting clients on server shutdown")
    (doseq [stream (map :out (vals @connections))]
      (try
        @(d/chain
          (s/put! stream {:type :server-disconnect
                          :message "Server shutting down"})
          (fn [_] (s/close! stream)))
        (catch Exception e
          (log/error e "Caught exception closing client"))))

    (when tcp-server
      (.close tcp-server))
    (sched/cancel scheduler :cull-connections)
    (assoc component :tcp-server nil, :connections nil)))


(defn tcp-server
  ([config] (tcp-server config (atom {})))
  ([{:keys [port ip cull-interval]} connections]
   (map->TcpServer {:port port
                    :ip ip
                    :cull-interval cull-interval
                    :connections connections})))
