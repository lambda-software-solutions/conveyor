(ns com.lambdasoftware.conveyor.server.trie
  "Trie for quickly locating leaves in the hieratchical path structure")

(defn- node
  ([] (node {} []))
  ([children] (node children []))
  ([children elements]
   {:children children :elements elements}))


(defn mk-trie [] (node))


(defn add-elements [n path xs]
  (if (empty? path)
    (update-in n [:elements] into xs)
    (update-in n [:children (first path)]
      (fnil #(add-elements % (rest path) xs) (node)))))


(defn add-element [n path x]
  (add-elements n path [x]))


(defn- empty-node? [n]
  (and (empty? (:children n))
       (empty? (:elements n))))


(defn remove-element [n path x]
  (let [{:keys [children elements]} n]
    (if (empty? path)
      (let [new-elems (into [] (remove #(= % x) elements))]
        (assoc n :elements new-elems))
      (if (or (empty-node? n)
              (nil? (get children (first path))))
        n
        (let [[segment & remainder] path
              child (get-in n [:children segment])
              new-child (remove-element child remainder x)]
          (if (and (empty-node? new-child))
            (update-in n [:children] dissoc segment)
            (update-in n [:children] assoc segment new-child)))))))


(defn collect
  "Collect all of the elements stored from the node passed
  in down to the element at path."
  [n path]
  (let [[segment & remainder] path
        xs (get n :elements [])
        child (get-in n [:children segment])]
    (if (nil? segment)
      xs
      (into xs (collect child remainder)))))
