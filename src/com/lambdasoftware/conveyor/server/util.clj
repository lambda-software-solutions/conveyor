(ns com.lambdasoftware.conveyor.server.util)

(defn paginate [page per-page xs]
  (->> xs
       (drop (* (dec page) per-page))
       (take per-page)))

