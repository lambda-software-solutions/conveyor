(ns com.lambdasoftware.conveyor.server.www.core
  (:require [clojure.string :refer [last-index-of]]
            [com.stuartsierra.component :as component]
            [com.lambdasoftware.conveyor.server.router.proto :as routes]
            [com.lambdasoftware.conveyor.server.router.sql :as sql]
            [com.lambdasoftware.conveyor.server.infr.subscriber-bus :as bus]
            [com.lambdasoftware.conveyor.server.www.subscriptions :as subscriptions]
            [clj-http.client :as http]
            [perseverance.core :as perseverance :refer [retriable retry]]
            [crypto.random :as rand]
            [manifold.deferred :as d]
            [yada.yada :as yada]
            [bidi.ring :refer (make-handler)]
            [schema.core :as schema]
            [taoensso.timbre :as log]
            [taoensso.nippy :as nippy]
            [clojure.java.jdbc :as jdb]
            [cemerick.url :as url])
  (:import (java.net Inet6Address InetSocketAddress)
           (java.io IOException)
           (org.postgresql.util PSQLException)))

(schema/defschema Event
  {(schema/optional-key :tx-id) Long
   :data {schema/Any schema/Any}})

(schema/defschema Action
  {:action (schema/enum "commit" "rollback")})

(defn to-query-param [[k v]]
  (str (url/url-encode (name k)) "=" (url/url-encode v)))

(defn with-query-params [url params]
  (str url (when-let [ps (seq params)]
             (str "?" (apply str (interpose "&" (map to-query-param ps)))))))

(defn remove-last-segment [url]
  (subs url 0 (last-index-of url \/)))

(defn is-top-level? [url]
  (boolean
   (re-matches #"/feeds/[^/]+$" url)))

(defn get-tx-events [conn tx-id]
  (some-> (jdb/query conn ["SELECT events FROM uncommitted_tx WHERE id = ?" tx-id])
          first
          (get :events)
          nippy/thaw))

(defn create-tx [ctx db]
  (let [conn (:conn db)
        tx-id (sql/next-tx-id conn)]
    (jdb/insert! conn :uncommitted_tx {:id tx-id})
    (-> (:response ctx)
        (assoc-in [:status] 201)
        (update-in [:headers] conj ["Location" (str "/transactions/" tx-id)]))))

(defn get-tx [ctx db]
  (when-let [tx-id (some-> (get-in ctx [:parameters :path :id]) Long.)]
    (get-tx-events (:conn db) tx-id)))

(defn append-event-to-tx [db feed evt]
  (jdb/with-db-transaction [tx (:conn db)]
    (let [tx-id (:tx-id evt)
          events (conj (or (get-tx-events tx tx-id) [])
                       [feed (:data evt)])]
      (try
        (jdb/execute! tx ["UPDATE uncommitted_tx SET events = ? WHERE id = ?"
                          (nippy/freeze events) tx-id])
        evt
        (catch Exception e
          (println "Error adding event to transaction (PSQL): " (.getMessage (.getNextException e)))
          nil)))))

(defn save-event [ctx router db]
  (let [evt (get-in ctx [:parameters :body])
        feed (get-in ctx [:parameters :path :feed])]
    (if (:tx-id evt)
      (if (append-event-to-tx db feed evt)
        evt
        (assoc (:response ctx) :status 500))
      (-> (routes/emit-event! router feed (nippy/freeze (:data evt)))
          (update-in [:data] nippy/thaw)))))

(defn apply-action-to-tx [ctx router db]
  (let [tx-id (some-> (get-in ctx [:parameters :path :id]) Long.)
        action (get-in ctx [:parameters :body :action])
        response (get ctx :response)]
    (case action
      "commit"
      (when-let [events (get-tx-events (:conn db) tx-id)]
        (routes/emit-events! router (map (fn [[path evt]]
                                           [path (nippy/freeze evt) tx-id])
                                         events))
        (jdb/execute! (:conn db) ["DELETE FROM uncommitted_tx WHERE id = ?" tx-id])
        (assoc response :status 204))

      "rollback"
      (let [deleted (jdb/execute! (:conn db) ["WITH deleted AS (DELETE FROM uncommitted_tx WHERE id = ? RETURNING *) SELECT count(*) AS num_deleted FROM deleted" tx-id])]
        (if (-> deleted first :num_deleted (= 1))
          (assoc response :status 204)
          (assoc response  :status 404)))

      :else (do
              (throw (ex-info "Unsupported action type"
                              {:action action
                               :tx-id tx-id}))
              {:status :error}))))

(defn pagination-links [base-url query page per-page total]
  (let [last-page (-> (/ total per-page) Math/ceil int)]
    (cond-> [{:rel "self" :href (with-query-params base-url query)}
             {:rel "first" :href (with-query-params base-url (assoc query :page 1))}
             {:rel "last" :href (with-query-params base-url (assoc query :page last-page))}]
      (> page 1)
      (conj {:rel "prev" :href (with-query-params base-url (assoc query :page (dec page)))})
      (< page last-page)
      (conj {:rel "next" :href (with-query-params base-url (assoc query :page (inc page)))}))))

(defn feed-handler [ctx router]
  (let [feed (get-in ctx [:parameters :path :feed])
        self-url (str "/feeds" feed)

        {:keys [page per-page]
         :or {page 1, per-page 100}
         :as query-params}
        (get-in ctx [:parameters :query])

        {:keys [total feeds] :as subfeeds}
        (routes/get-subfeeds router feed {:page page :per-page per-page})]
    {:_links (cond-> (pagination-links self-url query-params page per-page total)
               (not (is-top-level? self-url))
               (conj {:rel "parent" :href (remove-last-segment self-url)}))
     :subfeeds (map (fn [f] {:rel "child" :href (str "/feeds" (:feed f))}) (:feeds subfeeds))}))

(defn event-handler [ctx router]
  (let [feed (get-in ctx [:parameters :path :feed])
        {:keys [page per-page before-ts after-ts before-tx after-tx]
         :or {page 1
              per-page 100
              before-ts 0
              after-ts 0
              before-tx 0
              after-tx 0}
         :as query-params} (get-in ctx [:parameters :query])
        {:keys [total events]} (routes/get-feed router feed {:before-ts before-ts
                                                             :after-ts after-ts
                                                             :before-tx before-tx
                                                             :after-tx after-tx}
                                                            {:page page :per-page per-page})]
    {:_links (conj (pagination-links (str "/events" feed) query-params page per-page total)
                   {:rel "feed" :href (str "/feeds" feed)})
     :events (map #(update % :data nippy/thaw) events)}))
    

(defn feed-resource [router]
  (yada/resource
   {:id :resources/feed
    :description "Nested feeds of events"
    :produces [{:media-type "application/json"}
               {:media-type "application/edn"}]
    :parameters {:path {:feed String}}
    :methods {:post {:response #(routes/add-feed! router (get-in % [:parameters :path :feed]))}
              :get {:response #(feed-handler % router)
                    :parameters {:query {(schema/optional-key :page) schema/Int
                                         (schema/optional-key :per-page) schema/Int}}}}}))

(defn events-resource [router db]
  (yada/resource
   {:id :resources/event
    :description "Events for feed"
    :consumes [{:media-type "application/json"}
               {:media-type "application/edn"}]
    :produces [{:media-type "application/json"}
               {:media-type "application/edn"}]
    :methods {:post {:response #(save-event % router db)
                     :parameters {:body Event}}
              :get {:response #(event-handler % router)
                    :parameters {:query {(schema/optional-key :page) schema/Int
                                         (schema/optional-key :per-page) schema/Int
                                         (schema/optional-key :before-ts) schema/Int
                                         (schema/optional-key :after-ts) schema/Int
                                         (schema/optional-key :before-tx) schema/Int
                                         (schema/optional-key :after-tx) schema/Int}}}}}))

(defn transactions-resource [db]
  (yada/resource
   {:id :resources/transactions
    :description "Transactions encompassing multiple events"
    :consumes [{:media-type "application/json"}
               {:media-type "application/edn"}]
    :produces [{:media-type "application/json"}
               {:media-type "application/edn"}]
    :methods {:post {:response #(create-tx % db)}}}))

(defn transaction-resource [db]
  (yada/resource
   {:id :resources/transaction
    :description "Information for a single transaction"
    :produces [{:media-type "application/json"}
               {:media-type "application/edn"}]
    :methods {:get {:response #(get-tx % db)}}}))

(defn transaction-actions-resource [router db]
  (yada/resource
   {:id :resources/transaction
    :description "Transaction actions"
    :consumes [{:media-type "application/json"}
               {:media-type "application/edn"}]
    :produces [{:media-type "application/json"}
               {:media-type "application/edn"}]
    :methods {:post {:response #(apply-action-to-tx % router db)
                     :parameters {:body Action}}}}))

(defn get-handler [router db subscriber-bus]
  ["/" {"transactions" {"" (transactions-resource db)
                        ["/" :id] (transaction-resource db)
                        ["/" :id "/actions"] (transaction-actions-resource router db)}
        ["events" [#"/.+" :feed]] (events-resource router db)
        ["feeds" [#"/.+" :feed]] (feed-resource router)
        ["subscriptions" [#"(/.*)?" :feed]] (subscriptions/subscriptions-resource subscriber-bus)
        ["subscribers/" [#".+" :subscriber-url]] (subscriptions/subscribers-resource subscriber-bus)}])

(defrecord WebServer [port path router database subscriber-bus]
  component/Lifecycle
  (start [component]
    (log/info (format "Starting web server on port %d" port))
    (assoc component :server (yada/listener (get-handler router database subscriber-bus)
                                            {:port port :path path})))
  (stop [component]
    (log/info "Stopping web server")
    ((get-in component [:server :close]))
    (assoc component :server nil)))

(defn new-server [{:keys [port path]}]
  (map->WebServer {:port port
                   :path path}))
