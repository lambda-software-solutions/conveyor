(ns com.lambdasoftware.conveyor.server.www.subscriptions
    (:require [com.lambdasoftware.conveyor.server.infr.subscriber-bus :as bus]
              [clj-http.client :as http]
              [perseverance.core :as perseverance :refer [retriable retry]]
              [crypto.random :as rand]
              [manifold.deferred :as d]
              [yada.yada :as yada]
              [schema.core :as schema]
              [taoensso.timbre :as log]
              [taoensso.nippy :as nippy]
              [cemerick.url :as url])
    (:import (java.io IOException)))

(defn verify-challenge* [url]
  (let [challenge (rand/url-part 16)
        resp (http/get url {:query-params {:challenge challenge}})]
    (= challenge (:body resp))))

(defn verify-challenge [url]
  (retriable {} (verify-challenge* url)))
    
;; TODO: Immediately drop the subscription on a 404 response
(defn verify-with-default-retries [url]
  (retry {:strategy (perseverance/progressive-retry-strategy :initial-delay 2000
                                                             :max-delay 30000
                                                             :max-count 15)
          :log-fn (fn [ex attempt delay]
                    (log/debug (format "Webhook request to %s failed. Performing atempt %d after %dms"
                                       url attempt delay)))}
    (verify-challenge url)))
    
(defn subscriptions-handler [ctx subscriber-bus]
    (let [feed (get-in ctx [:parameters :path :feed])
          feed (if (.startsWith feed "/") (.substring feed 1) feed)
          url (get-in ctx [:parameters :body :url])
          response (get ctx :response)]
        (d/future
         (try
             (if (verify-with-default-retries url)
              (do (bus/subscribe subscriber-bus
                          (clojure.string/split feed #"/")
                          (bus/webhook-subscriber subscriber-bus url))
                  (assoc response :status 201))
              (assoc response :status 400
                              :body "Could not validate URL with challenge"))
             (catch IOException e
              (log/error e (format "Error requesting challenge from webhook URL: %s" url))
              (assoc response :status 400
                              :body "Could not connect to URL"))))))
    
    
(defn delete-subscription-handler [ctx subscriber-bus]
 (let [feed (get-in ctx [:parameters :path :feed])
       feed (if (.startsWith feed "/") (.substring feed 1) feed)
       url (get-in ctx [:parameters :body :url])
       response (get ctx :response)]
  (d/future
      (if (verify-with-default-retries url)
          (let [subscriber (bus/webhook-subscriber subscriber-bus url)]
           (if (empty? feed)
               (bus/unsubscribe-all subscriber-bus subscriber)
               (bus/unsubscribe subscriber-bus
                   (clojure.string/split feed #"/")
                   subscriber))
           (assoc response :status 202))))))


(defn get-subscriber-handler [ctx subscriber-bus]
    (d/future
        (let [{:keys [response]} ctx
              subs (as-> ctx $
                         (get-in $ [:parameters :path :subscriber-url])
                         (url/url-decode $)
                         (bus/webhook-subscriber subscriber-bus $)
                         (bus/get-subscriptions subscriber-bus $)
                         (into [] (map #(str "/" (clojure.string/join "/" %)) $)))]
            (if (empty? subs)
              (assoc response :status 404)
              (assoc response :status 200
                              :body subs)))))

(defn subscriptions-resource [subscriber-bus]
  (yada/resource
    {:id :resources/subscriptions
     :description "Webhook subscriptions that allow for clients to subscribe to updates"
     :consumes [{:media-type "application/json"}
                {:media-type "application/edn"}]
     :produces [{:media-type "application/json"}
                {:media-type "application/edn"}]
     :methods {:post {:response #(subscriptions-handler % subscriber-bus)
                      :parameters {:body {:url String}}}
               :delete {:response #(delete-subscription-handler % subscriber-bus)
                        :parameters {:body {:url String}}}}}))

(defn subscribers-resource [subscriber-bus]
  (yada/resource
    {:id :resources/subscribers
     :description "Webhook subscriber details"
     :consumes [{:media-type "application/json"}
                {:media-type "application/edn"}]
     :produces [{:media-type "application/json"}
                {:media-type "application/edn"}]
     :methods {:get {:response #(get-subscriber-handler % subscriber-bus)
                     :parameters {:path {:subscriber-url String}}}}}))
                      
