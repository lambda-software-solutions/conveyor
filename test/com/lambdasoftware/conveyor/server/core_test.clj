(ns com.lambdasoftware.conveyor.server.core-test
  (:require [clojure.test :refer :all]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]
            [com.lambdasoftware.conveyor.server.core :refer :all]))
