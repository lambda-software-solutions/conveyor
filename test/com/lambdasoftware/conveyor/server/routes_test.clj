(ns com.lambdasoftware.conveyor.server.routes-test
  (:refer-clojure :exclude [empty])
  (:require [clojure.test :refer :all]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [clojure.test.check.clojure-test :refer [defspec]]
            [com.gfredericks.test.chuck.generators :as gen']
            [com.lambdasoftware.conveyor.server.router.core :refer :all]
            [com.lambdasoftware.conveyor.server.router.proto :refer :all]))

(def valid-uri-path-part #"[-_.~A-Za-z0-9]+")
(def uri-parts
  (gen/not-empty
   (gen/vector (gen'/string-from-regex valid-uri-path-part))))

(defn fake-event []
  {:id (.toString (java.util.UUID/randomUUID))})

(defmacro throws? [body]
  `(try ~body
        false
        (catch Throwable _#
          true)))

(defspec splits-url-into-path-vector
  100
  (prop/for-all [parts uri-parts]
    (= parts (segments (str "/" (clojure.string/join "/" parts))))))

(defspec segments-requires-ring-uri-format
  50
  (prop/for-all [parts uri-parts]
    (throws? (segments (clojure.string/join "/" parts)))))

(comment)
(defspec router-round-trip-feed
  100
  (let [r (new-router {:type :memory})]
    (prop/for-all [parts uri-parts]
                  (add-feed! r parts)
                  (= {:events [] :total 0}
                     (get-feed r parts {} {:page 1 :per-page 10})))))

(deftest returns-empty-when-none-matched
  (is (= {:events '() :total 0}
         (get-feed (new-router {:type :memory}) "/some/path" {} {:page 1 :per-page 10}))))

(comment
  (defspec router-gets-event-emitted
    100
    (let [r (new-router {:type :memory})
          e (fake-event)]
      (prop/for-all [parts uri-parts]
                    (add-feed! r parts)
                    (emit-event! r parts e)
                    (= [(event e)] (get-feed r parts {} {:page 1 :per-page 10}))))))

(deftest gets-subfeeds
  (let [r (new-router {:type :memory})]
    (add-feed! r ["parent" "c1"])
    (add-feed! r ["parent" "c2"])
    (add-feed! r ["parent" "c1" "grandchild"])
    (is (= {:total 2
            :feeds [{:type :events/feed-ref :url "/parent/c1"}
                    {:type :events/feed-ref :url "/parent/c2"}]}
           (get-subfeeds r ["parent"] {:page 1 :per-page 10})))))

(comment
  (run-tests))
