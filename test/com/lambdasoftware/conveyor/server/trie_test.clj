(ns com.lambdasoftware.conveyor.server.trie-test
  (:require [clojure.test :refer :all]
            [com.lambdasoftware.conveyor.server.trie :as trie]))

(testing "Building tries"
  (deftest trie-creation
    (is (= {:children {} :elements []}
           (trie/mk-trie))))

  (deftest adding-element-to-root
    (is (= {:children {} :elements [:elem1]}
           (-> (trie/mk-trie) (trie/add-element [] :elem1)))))

  (deftest adding-element-to-existing-nested-path
    (is (= {:children {"first" {:children {} :elements [:elem1]}}
            :elements []}
           (-> {:children {"first" {:children {} :elements []}}
                :elements []}
               (trie/add-element ["first"] :elem1)))))

  (deftest adding-element-to-new-nested-path
    (is (= {:children {"first" {:children {} :elements [:elem1]}}
            :elements []}
           (-> (trie/mk-trie)
               (trie/add-element ["first"] :elem1)))))

  (deftest adding-multiple-elements
    (is (= {:children {} :elements [:elem1 :elem2]}
           (-> (trie/mk-trie)
               (trie/add-elements [] [:elem1 :elem2])))))

  (deftest appending-elements
    (is (= {:children {} :elements [:exists1 :added1 :added2 :added3]}
           (-> {:children {} :elements [:exists1]}
               (trie/add-element [] :added1)
               (trie/add-elements [] [:added2 :added3])))))

  (deftest removing-elements
    (is (= (-> (trie/mk-trie)
               (trie/add-element ["test"] :elem1)
               (trie/add-element ["test"] :elem2)
               (trie/remove-element ["test"] :elem2))
           (-> (trie/mk-trie)
               (trie/add-element ["test"] :elem1)))))

  (deftest removing-nested-element-removes-empty-parent
    (is (= (-> (trie/mk-trie)
               (trie/add-element ["test"] :elem1))
           (-> (trie/mk-trie)
               (trie/add-element ["test"] :elem1)
               (trie/add-element ["test" "a" "b" "c"] :elem2)
               (trie/remove-element ["test" "a" "b" "c"] :elem2)))))

  (deftest removing-element-does-not-remove-node-with-other-elements
    (is (= (-> (trie/mk-trie)
               (trie/add-element ["test"] :elem1))
           (-> (trie/mk-trie)
               (trie/add-elements ["test"] [:elem1 :elem2])
               (trie/remove-element ["test"] :elem2)))))

  (deftest removing-nonexistent-element-does-nothing
    (is (= (-> (trie/mk-trie)
               (trie/add-element ["test"] :elem1))
           (-> (trie/mk-trie)
               (trie/add-element ["test"] :elem1)
               (trie/remove-element ["test"] :elem2)))))

  (deftest removing-element-at-nonexistent-path-does-nothing
    (is (= (-> (trie/mk-trie)
               (trie/add-element ["test"] :elem1))
           (-> (trie/mk-trie)
               (trie/add-element ["test"] :elem1)
               (trie/remove-element ["does" "not" "exist"] :elem2)))))

  (deftest removing-elements-does-not-prune-sibling-branch
    (is (=  (-> (trie/mk-trie)
                (trie/add-element ["a" "b"] :elem1))
            (-> (trie/mk-trie)
                (trie/add-element ["a" "b"] :elem1)
                (trie/add-element ["a" "c"] :elem2)
                (trie/remove-element ["a" "c"] :elem2)))))

  (deftest removing-element-at-one-path-does-not-remove-it-at-other
    (is (= (-> (trie/mk-trie)
               (trie/add-element ["a"] :elem1)
               (trie/add-element ["a" "b" "c"] :elem1))
           (-> (trie/mk-trie)
               (trie/add-element ["a"] :elem1)
               (trie/add-element ["a" "b"] :elem1)
               (trie/add-element ["a" "b" "c"] :elem1)
               (trie/remove-element ["a" "b"] :elem1))))))


(testing "Searching tries"
  (deftest collecting-root-returns-root-elems
    (is (= [:elem1 :elem2]
           (-> {:children {} :elements [:elem1 :elem2]}
               (trie/collect [])))))

  (deftest collecting-returns-all-elems-from-root
    (is (= [:elem1 :elem2 :elem3 :elem4]
           (-> (trie/mk-trie)
               (trie/add-elements [] [:elem1 :elem2])
               (trie/add-element ["foo"] :elem3)
               (trie/add-element ["foo" "bar"] :elem4)
               (trie/collect ["foo" "bar"])))))

  (deftest collecting-elides-elements-not-in-path
    (is (= [:elem1 :elem2 :elem4]
           (-> (trie/mk-trie)
               (trie/add-element [] :elem1)
               (trie/add-element ["foo"] :elem2)
               (trie/add-element ["other"] :elem3)
               (trie/add-element ["foo" "bar"] :elem4)
               (trie/collect ["foo" "bar"])))))

  (deftest collecting-collects-as-far-as-paths-materialized
    (is (= [:elem1 :elem2]
           (-> (trie/mk-trie)
               (trie/add-element ["some"] :elem1)
               (trie/add-element ["some" "deeply"] :elem2)
               (trie/collect ["some" "deeply" "nested" "path"]))))))

(run-tests)
